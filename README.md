# BYU Holographic Video Monitor Stereograms

Generates combined modulated tones with an analog graphics card for displaying N-view stereograms on the BYU Holographic Video Monitor.

It has three windows:
 1) a debug message console window, which accepts no input
 2) a GUI, which accepts both mouse and keyboard input when in focus
 3) an output window, which should be fullscreened on the desired GPU output.

This program was written for analog-output-capable NVIDIA (R) graphics cards, which typically have a GPU DAC/sample/pixel clock rate of 400MHz. This results in a radio-frequency bandwidth of 200MHz. The variable `pixelClockMHz` should be adjusted if the GPU pixel clock is different.

Keypresses for when the GUI is in focus:
 * LEFT - decrease last adjusted slider by 1%
 * RIGHT - increase last adjusted slider by 1%
 * CTRL/ALT/SHIFT - each additional modifier multiplies the LEFT/RIGHT delta by 0.1. Thus, holding down all three modifiers while pressing either LEFT/RIGHT effects a change of 0.001% of the slider's range.
 *

Keypresses for when an output window is in focus:
 * 'f' - toggle fullscreen of this output window.
 
NOTE: If not yet implemented, you'll need to edit the ofxDatGui source file "ofxDatGui/src/components/ofxDatGuiSlider.h" to include the following functions:

```
double getScale()
{
  return mScale;
}

double getMax() // added
{
  return mMax;
}

double getMin() // added
{
  return mMin;
}
```

Implemented with:
 * OpenFrameworks v0.9.3
 * OpenFrameworks addon "ofxDatGui" by Stephen Braitsch
 * OpenFrameworks addon "ofxJSON" by Jeff Crouse, Christopher Baker, Andreas Müller
 * Fafers Technical Font by Fábian Fafers, (C) 2010 (if included)
 * Microsoft Visual Studio Professional 2015
 * Microsoft Windows 7 / 10
 * NVIDIA (R) Quadro (R) FX 5800 graphics card, for its dual DVI-__**I**__ outputs

NOTE: FAFERS_Technical_Font.ttf can be freely downloaded from several font websites, but until written permission has been received from the author, it is not included with this distribution (it will be if an enabling response is received). You can download the file and copy it into "/bin/data/" if you wish.
 
# License

All files in this repository, including source code and auxiliary references, with the exception of the below attributions of external works, are Copyright 2014-2019 BYU ElectroHolography Research Group and are licensed under GNU General Public License v3 ([https://www.gnu.org/licenses/gpl-3.0.en.html](https://www.gnu.org/licenses/gpl-3.0.en.html)).

Attributions of external work:
 * Base code for screen capture as contributed by OpenFrameworks Forum users "msf567" and "lilive"
 * FAFERS_Technical_Font.ttf by Fábian Fafers, (C) 2010 (if included)
 * ofxDatGui OpenFrameworks addon source code by Stephen Braitsch
 * ofxJSON OpenFrameworks addon source code by Jeff Crouse, Christopher Baker, Andreas Müller
 * OpenFrameworks source code by respective contributing authors

![GPL v3 Logo](https://www.gnu.org/graphics/gplv3-127x51.png "GPL v3")
