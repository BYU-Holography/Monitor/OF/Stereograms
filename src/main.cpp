/*
BYU Holographic Video Monitor Stereograms
Generates combined modulated tones via an analog graphics card
for displaying N-view stereograms on the BYU Holographic Video Monitor.

Copyright 2014-2019 BYU ElectroHolography Research Group

Email: byuholography@gmail.com

Attributions of external work:
* FAFERS_Technical_Font.ttf by F�bian Fafers, (C) 2010 (if included)
* ofxDatGui OpenFrameworks addon by Stephen Braitsch
* ofxJSON OpenFrameworks addon by Jeff Crouse, Christopher Baker, Andreas M�ller
* OpenFrameworks by respective contributing authors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ofMain.h"

// http://www.openframeworks.cc/tutorials/graphics/shaders.html
// import the fancy new renderer
#include "ofGLProgrammableRenderer.h"
#include "ofAppGLFWWindow.h"

#include "GUIApp.h"
#include "outputApp.h"

//========================================================================
int main()
{

	//ofGLWindowSettings settings;
	ofGLFWWindowSettings settings; // wrapper for ofGLWindowSettings?
	                               // needed for settings.shareContextWith parameter
	                               // see big comment on multiwindow-shared ofFbo's


	// setup GUI window

	settings.setGLVersion( 3, 2 );
	// settings.width = 400;
	// settings.height = 400;
	settings.setPosition( ofVec2f( 10, 550 ) );


	shared_ptr<ofAppBaseWindow> gui_window = ofCreateWindow( settings );
	shared_ptr<GUIApp> gui_app( new GUIApp );


	// setup output window

	settings.shareContextWith = gui_window; // necessary to share ofFbo's from the GUI with the output app
	                                        // see https://github.com/openframeworks/openFrameworks/pull/4565
	                                        // and big comment in outputApp.cpp
	shared_ptr<ofAppBaseWindow> output_window0 = ofCreateWindow( settings );
	shared_ptr<outputApp> output_app0( new outputApp );


	// make links between the different apps, initialize other variables

	output_app0->gui_app = gui_app;
	output_app0->output_window_index = 0;

	gui_app->output_app0 = output_app0;
	gui_app->output_window_count = 1; // NUMBER OF OUTPUT WINDOWS

	gui_app->gui_window = gui_window;
	gui_app->output_window0 = output_window0;



	// run

	ofRunApp( gui_window, gui_app );
	gui_window->setWindowShape( 875 + gui_app->gui0->getWidth(), gui_app->gui0->getHeight() + 100 );

	//output_window0->setWindowPosition( 10, gui_window->getWindowPosition().y + gui_window->getHeight() + 40 );
	ofRunApp( output_window0, output_app0 );

	ofRunMainLoop();

}
