#include "GUIApp.h"



//--------------------------------------------------------------
void GUIApp::loadStereogramFromFile( string file_path )
{
  ofColor previous_color = ofGetCurrentRenderer()->getStyle().color;

  printf( "Loading from %s...\n", file_path.c_str() );

  ofFile file( file_path );

  ofxJSONElement config;

  char temp_char_array[256];
  string temp_string;
  float temp_float;
  int temp_int;
  bool temp_bool;

  int temp_reload_period;



  if( file.exists() )
  {
    printf( "file exists\n" );

    if( ofToUpper( file.getExtension() ) == "STO" )
    {
      printf( "is .sto file\n" );

      json_config_file_directory = file.getEnclosingDirectory();
      printf( "directory: %s\n", json_config_file_directory.c_str() );

      config.open( file_path );

      printf( "%s\n\n", config.getRawString().c_str() );



      // parse JSON data

      // TODO: set default values if tag is unavailable (ie, it's an older JSON file version)
      // use config["blah"].isNull()?

      if( config["title"].isNull() )
        temp_string = "Untitled";
      else
        temp_string = config["title"].asString();
      gui0_general_Title->setText( temp_string );


      // load stereogram_mode
      int temp_int;
      for( temp_int = 0; temp_int < mode_options.size(); temp_int++ )
      {
        if( !config["stereogram_mode"].isNull() && // with && operators, if the first arg evaluates to false, the 2nd arg isn't evaluated
            ofToUpper( mode_options[temp_int] ) == ofToUpper( config["stereogram_mode"].asString() )
            )
          break;
      }
      if( temp_int < mode_options.size() )
      {
        stereogram_mode = (StereogramMode) temp_int;
      }
      else
      {
        // invalid option; default to static image stereogram
        stereogram_mode = GUIApp::POLY_STATIC;
      }
      gui0_general_StereogramMode->select( stereogram_mode );


      // load output_gain
      if( config["output_gain"].isNull() )
        temp_float = 1.0;
      else
        temp_float = config["output_gain"].asFloat();
      gui0_general_OutputGain->setValue( temp_float );


      // load flip_horizontal
      if( config["flip_horizontal"].isNull() )
        temp_bool = false;
      else
        temp_bool = config["flip_horizontal"].asString() == "T";
      gui0_general_FlipHorizontal->setChecked( temp_bool ); // how can we do bools better?


      // load flip_vertical
      if( config["flip_vertical"].isNull() )
        temp_bool = false;
      else
        temp_bool = config["flip_vertical"].asString() == "T";
      gui0_general_FlipVertical->setChecked( temp_bool );


      // load roll_vertical
      if( config["roll_vertical"].isNull() )
        temp_bool = false;
      else
        temp_bool = config["roll_vertical"].asString() == "T";
      gui0_general_RollVertical->setChecked( temp_bool );


      // load output_image_width
      if( config["output_image_width"].isNull() )
        temp_string = "600";
      else
        temp_string = config["output_image_width"].asString();
      gui0_general_OutputImageWidth->setText( temp_string );


      // load n_slices
      if( config["slices"].isNull() )
        n_slices = 1;
      else
        n_slices = config["slices"].size();
      // the widget relevant to n_slices will be set later on after checks on n_slices have been made


      // load reload_period
      if( config["reload_period"].isNull() )
        temp_reload_period = 0;
      else
        temp_reload_period = config["reload_period"].asInt();
      // the widget relevant to reload_period will be set later on after checks on reload_period have been made



      // apply effects from the specific mode that we're in
      switch( stereogram_mode )
      {
      case GUIApp::POLY_STATIC:

        // enable slice-count flexibility
        gui0_general_SliceCount->setVisible( true );

        // enable reload period flexibility
        gui0_general_ReloadPeriod->setVisible( true );

        break;
      case GUIApp::MONO_VIDEO:

        // force number of slices to 1
        n_slices = 1;
        gui0_general_SliceCount->setVisible( false );

        // force reload stop
        temp_reload_period = 0;
        gui0_general_ReloadPeriod->setVisible( false );

        break;
      case GUIApp::QUAD_VIEW:

        // force number of slices to 4
        n_slices = 4;
        gui0_general_SliceCount->setVisible( false );

        // force reload stop
        temp_reload_period = 0;
        gui0_general_ReloadPeriod->setVisible( false );

        break;
      case GUIApp::MONO_SCREEN:

        // force number of slices to 1
        n_slices = 1;
        gui0_general_SliceCount->setVisible( false );

        // force reload stop
        temp_reload_period = 0;
        gui0_general_ReloadPeriod->setVisible( false );

        break;
      }


      for( int i = 0; i < n_slices; i++ )
      {

        if( config["slices"][i]["source"].isNull() )
          slices[i].source = "not specified";
        else
          slices[i].source = config["slices"][i]["source"].asString();


        if( config["slices"][i]["enabled"].isNull() )
          slices[i].enabled = false;
        else
          slices[i].enabled = config["slices"][i]["enabled"].asString() == "T";


        if( config["slices"][i]["blanking"].isNull() )
          slices[i].blanking = false;
        else
          slices[i].blanking = config["slices"][i]["blanking"].asString() == "T";


        if( config["slices"][i]["y_offset"].isNull() )
          slices[i].y_offset = 0;
        else
          slices[i].y_offset = config["slices"][i]["y_offset"].asInt();


        if( config["slices"][i]["angle"].isNull() )
          slices[i].angle = 0.0;
        else
          slices[i].angle = config["slices"][i]["angle"].asFloat();


        if( config["slices"][i]["freq_r"].isNull() )
          slices[i].freq_r = pixelClockMHz * 0.5 * 0.25;
        else;
        slices[i].freq_r = config["slices"][i]["freq_r"].asFloat();


        if( config["slices"][i]["freq_g"].isNull() )
          slices[i].freq_g = pixelClockMHz * 0.5 * 0.5;
        else
          slices[i].freq_g = config["slices"][i]["freq_g"].asFloat();


        if( config["slices"][i]["freq_b"].isNull() )
          slices[i].freq_b = pixelClockMHz * 0.5 * 0.75;
        else
          slices[i].freq_b = config["slices"][i]["freq_b"].asFloat();


        if( config["slices"][i]["ampl_r"].isNull() )
          slices[i].ampl_r = 1.0;
        else
          slices[i].ampl_r = config["slices"][i]["ampl_r"].asFloat();


        if( config["slices"][i]["ampl_g"].isNull() )
          slices[i].ampl_g = 1.0;
        else
          slices[i].ampl_g = config["slices"][i]["ampl_g"].asFloat();


        if( config["slices"][i]["ampl_b"].isNull() )
          slices[i].ampl_b = 1.0;
        else
          slices[i].ampl_b = config["slices"][i]["ampl_b"].asFloat();
      }



      // update widgets; TODO: eliminate code redundancy with onTextInputEvent( ofxDatGuiTextInputEvent e )
      if( focused_slice >= n_slices )
      {
        focused_slice = n_slices - 1;
      }
      if( focused_slice_last >= n_slices )
      {
        focused_slice_last = n_slices - 1;
      }

      // update slice count indicator
      sprintf( temp_char_array, "%i", n_slices );
      gui0_general_SliceCount->setText( temp_char_array );

      // update slice selection drop-down menu
      vector<string> selected_slice_options;
      for( int i=0; i < n_slices; i++ )
      {
        sprintf( temp_char_array, "Slice %u", i );
        selected_slice_options.push_back( temp_char_array );
      }
      gui0_slice_SelectedSlice->setNewOptions( selected_slice_options );

      // update reload period (POLY_STATIC mode only)
      sprintf( temp_char_array, "%i", temp_reload_period );
      gui0_general_ReloadPeriod->setText( temp_char_array );



      //update widgets from focused slice
      updateWidgetsFromSlice( focused_slice );



      // load image files
      loadSlicesFromFiles();
    }
    else
    {
      printf( "this isn't an STO (stereogram) file\n" );
    }
  }
  else
  {
    printf( "that doesn't exist\n" );
  }

  ofSetColor( previous_color );
}



//--------------------------------------------------------------
void GUIApp::loadSlicesFromFiles()
{
  // perform a survey of the new image widths before drawing

  // load the images once by loading them into a temporary array

  string image_didnt_load_message = "image didn't load";

  unsigned int max_image_width = 1; // have something just in case something doesn't go right

  ofImage* temp_images = NULL;



  // TODO: should the video player stop code be here?
  if( video_player.isPlaying() )
    video_player.stop();

  if( video_player.isLoaded() )
    video_player.closeMovie();



  // profile slice data...

  switch( stereogram_mode )
  {
  case GUIApp::POLY_STATIC:
  {
    temp_images = new ofImage[n_slices];

    for( int i = 0; i < n_slices; i++ )
    {
      if( temp_images[i].load( json_config_file_directory + slices[i].source ) )
      {
        // load successful
        max_image_width = max(
          max_image_width,
          (unsigned int) temp_images[i].getWidth()
        );
      }
      else
      {
        // load not successful; prepare to print an error message
        max_image_width = max(
          max_image_width,
          (unsigned int) ( font_big.getStringBoundingBox( image_didnt_load_message, 0.0, 0.0, true ).width + 5 )
          // give a little bit of extra space on the end
        );
      }
    }

    // TODO: downsize the images if they're too big
  }
  break;

  case GUIApp::MONO_VIDEO:

    video_player.load( json_config_file_directory + slices[0].source );

    video_player.update(); // get a frame

    max_image_width = video_player.getPixelsRef().getWidth();
    //video_player.getPixelsRef().getHeight();

    // TODO: make sure the video doesn't have ridiculous dimensions, ie, 4k video

    break;
  }



  // deallocate (destroy) sources_fbo for resizing
  // (we'd only be able to keep the current sources_fbo
  //  if the sources width and number of slices didn't change)

  if( sources_fbo.isAllocated() )
    sources_fbo.destroy();

  sources_fbo.allocate( max_image_width, n_slices * N_HOLOLINES, GL_RGB );


  // check to see if we need to change 'output image width'
  {
    int output_image_width = atoi( gui0_general_OutputImageWidth->getText().c_str() );

    if( max_image_width > output_image_width )
    {
      char temp_char_array[16];
      sprintf( temp_char_array, "%i", max_image_width );
      gui0_general_OutputImageWidth->setText( temp_char_array );
    }
  }



  // start using slice data...

  switch( stereogram_mode )
  {
  case GUIApp::POLY_STATIC:
    // now that we've got our FBO allocated, start drawing to it...

    // draw to the FBO
    sources_fbo.begin();

    for( int i = 0; i < n_slices; i++ )
    {
      if( temp_images[i].isAllocated() )
      {
        ofSetColor( 255 );
        temp_images[i].draw( 0.0, N_HOLOLINES * i );
      }
      else
      {
        // file didn't load successfully; draw an error message in its place (***this error message***)
        ofSetColor( 255, 250, 0 );
        font_big.drawString( image_didnt_load_message, 0.0, N_HOLOLINES * i + font_big.getLineHeight() );
      }
    }

    sources_fbo.end();
    // we're done drawing to the FBO
    break;

  case GUIApp::MONO_VIDEO:

    // start video playback
    video_player.setLoopState( OF_LOOP_NORMAL );
    video_player.play();

    break;

  case GUIApp::QUAD_VIEW:
    break;

  case GUIApp::MONO_SCREEN:
    break;
  }



  // clear & redraw source images and slice config printouts
  redraw_slices = true;



  // free up temporary image array now that the images are copied to the FBO
  if( temp_images != NULL )
    delete[] temp_images;
}



//--------------------------------------------------------------
void GUIApp::saveStereogramToFile( string file_path )
{
  printf( "Saving to %s... ", file_path.c_str() );

  ofxJSONElement config;

  config["PIXELS_H_ACTIVE"] = PIXELS_H_ACTIVE;
  config["PIXELS_V_ACTIVE"] = PIXELS_V_ACTIVE;
  config["COMP_ROWS_PER_HOLOLINE"] = COMP_ROWS_PER_HOLOLINE;
  config["N_HOLOLINES"] = N_HOLOLINES;
  config["pixel_clock_MHz"] = pixelClockMHz;

  config["title"] = gui0_general_Title->getText();
  config["output_gain"] = gui0_general_OutputGain->getValue();
  config["flip_horizontal"] = gui0_general_FlipHorizontal->getChecked() ? "T" : "F"; // how can we do bools better?
  config["flip_vertical"] = gui0_general_FlipVertical->getChecked() ? "T" : "F";
  config["roll_vertical"] = gui0_general_RollVertical->getChecked() ? "T" : "F";
  config["output_image_width"] = atoi( gui0_general_OutputImageWidth->getText().c_str() );
  config["reload_period"] = atoi( gui0_general_ReloadPeriod->getText().c_str() );
  config["stereogram_mode"] = gui0_general_StereogramMode->getLabel();

  for( int i=0; i < n_slices; i++ )
  {
    config["slices"][i]["source"] = slices[i].source;
    config["slices"][i]["enabled"] = slices[i].enabled ? "T" : "F";
    config["slices"][i]["blanking"] = slices[i].blanking ? "T" : "F";
    config["slices"][i]["y_offset"] = slices[i].y_offset;
    config["slices"][i]["angle"] = slices[i].angle;
    config["slices"][i]["freq_r"] = slices[i].freq_r;
    config["slices"][i]["freq_g"] = slices[i].freq_g;
    config["slices"][i]["freq_b"] = slices[i].freq_b;
    config["slices"][i]["ampl_r"] = slices[i].ampl_r;
    config["slices"][i]["ampl_g"] = slices[i].ampl_g;
    config["slices"][i]["ampl_b"] = slices[i].ampl_b;
  }

  config.save( file_path, true );

  printf( "done.\n" );
}