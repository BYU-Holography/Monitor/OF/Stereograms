#include "outputApp.h"

//--------------------------------------------------------------
void outputApp::setup()
{
#ifdef TARGET_OPENGLES
	shader.load( "shadersES2/shader" ); // we're in Android / iOS ?
	ofLog() << "shadersES2 used";
#else
	if( ofIsGLProgrammableRenderer() )
	{
		shader.load( "shadersGL3/shader" ); // newer version of openGL
		ofLog() << "shadersGL3 (three) used"; // http://www.openframeworks.cc/documentation/utils/ofLog.html
	}
	else
	{
		shader.load( "shadersGL2/shader" ); // older version of openGL
		ofLog() << "shadersGL2 (two) used";
	}
#endif

	// output_window_index = 0; // set in main.cpp

	char temp_c_str[32];
	sprintf( temp_c_str, "Output Window %u", output_window_index);
	ofSetWindowTitle( temp_c_str );

	ofSetColor( 255 );
}

//--------------------------------------------------------------
void outputApp::update()
{
	// rearrange the slices array for transmission to the GPU
	// "de-struct"
	for( int i = 0; i < gui_app->n_slices; i++ )
	{
		enabled[i]  = gui_app->slices[i].enabled; // 1-bit bool to int conversion
		blanking[i] = gui_app->slices[i].blanking; // 1-bit bool to int conversion

		y_offset[i] = gui_app->slices[i].y_offset; // short int to int conversion

		freq_k_r[i] = gui_app->slices[i].freq_r * gui_app->inv_pixelClockMHz_2_PI;
		freq_k_g[i] = gui_app->slices[i].freq_g * gui_app->inv_pixelClockMHz_2_PI;
		freq_k_b[i] = gui_app->slices[i].freq_b * gui_app->inv_pixelClockMHz_2_PI;

		ampl_r[i]   = gui_app->slices[i].ampl_r;
		ampl_g[i]   = gui_app->slices[i].ampl_g;
		ampl_b[i]   = gui_app->slices[i].ampl_b;
	}

	
	shader.begin();
	// all this code from shader.begin() to shader.end() can be put in the draw() function,
	// but there miiight be some optimization improvements if it's put in here...
	// see https://forum.openframeworks.cc/t/update-vs-draw/776

	// correlating to the order of variables defined in shader.frag:

	shader.setUniform1i( "PIXELS_H_ACTIVE", gui_app->PIXELS_H_ACTIVE ); // unsigned int to int conversion
	shader.setUniform1i( "PIXELS_V_ACTIVE", gui_app->PIXELS_V_ACTIVE ); // unsigned int to int conversion
	shader.setUniform1i( "COMP_ROWS_PER_HOLOLINE", gui_app->COMP_ROWS_PER_HOLOLINE ); // unsigned int to int conversion
	shader.setUniform1i( "N_HOLOLINES", gui_app->N_HOLOLINES ); // unsigned int to int conversion

	unsigned int n_slices = gui_app->n_slices;
	shader.setUniform1i( "n_slices", n_slices ); // unsigned int to int conversion
	shader.setUniform1f( "output_gain", gui_app->gui0_general_OutputGain->getValue() ); // double to float conversion
	shader.setUniform1i( "flip_horizontal", gui_app->gui0_general_FlipHorizontal->getChecked() ); // bool to int conversion
	shader.setUniform1i( "flip_vertical", gui_app->gui0_general_FlipVertical->getChecked() ); // bool to int conversion
	shader.setUniform1i( "roll_vertical", gui_app->gui0_general_RollVertical->getChecked() ); // bool to int conversion
	shader.setUniform1i( "output_image_width", atoi( gui_app->gui0_general_OutputImageWidth->getText().c_str() ) );


	shader.setUniformTexture( "tex0", gui_app->sources_fbo.getTextureReference(), 0 );
	// the 3rd argument, the '0' on the end, refers to a memory location on the GPU and needs to be different
	// for each texture; if it's the same as in other shader.setUniformTexture(...) calls, those textures will
	// be overwritten
	/*
	Note: there was a horrible 'bug' where accessing gui_app->sources_fbo would access the width/height/etc...
	members correctly, but the image data would always show up as black.
	It turns out that the gui_app and the output_app0 had their own openGL contexts (or something like that)
	that prevented sharing image data. After reading https://github.com/openframeworks/openFrameworks/pull/4565, which
	I found by just browsing the open issues on OpenFramework's GitHub site, looking for things related to ofFbo's
	and multiple windows, I inspected the example project of_v0.9.3_vs_release\examples\events\multiWindowOneAppExample
	and copying the lines
	ofGLFWWindowSettings settings;
	...
	settings.shareContextWith = mainWindow;
	and adapting them to this project's context
	After doing this (see main.cpp), IT WORKED!!!
	*/
	shader.setUniform1i( "source_image_width", gui_app->sources_fbo.getWidth() ); // float to int conversion
																																								// setting a variable such as source_image_height is unnecessary because it's equal to n_slices * N_HOLOLINES


	shader.setUniform1iv( "enabled", enabled, n_slices );
	shader.setUniform1iv( "blanking", blanking, n_slices );

	shader.setUniform1iv( "y_offset", y_offset, n_slices );

	shader.setUniform1fv( "freq_k_r", freq_k_r, n_slices );
	shader.setUniform1fv( "freq_k_g", freq_k_g, n_slices );
	shader.setUniform1fv( "freq_k_b", freq_k_b, n_slices );

	shader.setUniform1fv( "ampl_r", ampl_r, n_slices );
	shader.setUniform1fv( "ampl_g", ampl_g, n_slices );
	shader.setUniform1fv( "ampl_b", ampl_b, n_slices );


	//shader.setUniform1i( "outputMode", gui->outputMode ); // TODO: evaluate if these are necessary
	//shader.setUniform1i( "outputChannel", gui->outputChannel );
	//shader.setUniform1i( "mixingMode", gui->sharedOutputMixingMode );

	shader.end();
}

//--------------------------------------------------------------
void outputApp::draw()
{

	if( gui_app->set_output_window_fullscreen_oneshot )
	{

		gui_app->output_window0_isFullScreen = true;

		gui_app->setOutputWindowFullscreen( gui_app->output_window0, gui_app->output_window0_isFullScreen );
		// this is probably not the best way to do this

		gui_app->set_output_window_fullscreen_oneshot--;
	}


	// it's necessary for the color set by ofSetColor(255) to be 255 / white
	// otherwise everything will be dimmer / black
	shader.begin();
	ofRect( 0, 0, ofGetWidth(), ofGetHeight() );
	shader.end();

	output_framerate = ofGetFrameRate(); // is this the same framerate as in the GUI?
}

//--------------------------------------------------------------
void outputApp::windowResized( int w, int h )
{
	printf( "output window %u now %ix%i at (%i,%i)\n", output_window_index, w, h, ofGetWindowPositionX(), ofGetWindowPositionY() );
}

//--------------------------------------------------------------
void outputApp::keyPressed( int key )
{
	switch( key )
	{
	case 'f':
		ofToggleFullscreen();
		break;
	case ' ':
		ofSetFullscreen( false );
		ofSetWindowShape( 800, 600 );
		ofSetWindowPosition( 10, 30 );
		break;
	}
}

/*
//--------------------------------------------------------------
void outputApp::keyReleased(int key){

}

//--------------------------------------------------------------
void outputApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void outputApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void outputApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void outputApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void outputApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void outputApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void outputApp::dragEvent(ofDragInfo dragInfo){

}

//--------------------------------------------------------------
void outputApp::gotMessage( ofMessage msg )
{

}
*/
void outputApp::exit()
{
  printf( "OutputApp %u: BYE.\n", output_window_index );

  gui_app->exit();
}